import Logger from './Logger.ets'
import EmptyLogger from './EmptyLogger.ets'
import VerboseHarmonyLogger from './VerboseHarmonyLogger.ets'
import InfoHarmonyLogger from './InfoHarmonyLogger.ets'
import WarningHarmonyLogger from './WarningHarmonyLogger.ets'

/**
 * Static factory methods for getting different {@link org.altbeacon.beacon.logging.Logger}
 * implementations.
 */
class Loggers {
/** Empty Logger Singleton. */
  private static readonly EMPTY_LOGGER: Logger = new EmptyLogger();

/** Debug Logger Singleton. */
  private static readonly VERBOSE_HARMONY_LOGGER: Logger = new VerboseHarmonyLogger();

/** Info Logger Singleton. */
  private static readonly INFO_HARMONY_LOGGER: Logger = new InfoHarmonyLogger();

/** Warning Logger Singleton. */
  private static readonly WARNING_HARMONY_LOGGER: Logger = new WarningHarmonyLogger();

/**
     * @return Get a logger that does nothing.
     */
  public static empty(): Logger {
    return Loggers.EMPTY_LOGGER;
  }

/**
     * @return Get a logger that logs all messages to default logs.
     */
  public static verboseLogger(): Logger {
    return Loggers.VERBOSE_HARMONY_LOGGER;
  }

/**
     * @return Get a logger that logs messages of info and greater.
     */
  public static infoLogger(): Logger {
    return Loggers.INFO_HARMONY_LOGGER;
  }

/**
     * @return Get a logger that logs messages of warning and greater.
     */
  public static warningLogger(): Logger {
    return Loggers.WARNING_HARMONY_LOGGER;
  }

  private Loggers() {
    // No instances
  }
}

export default Loggers