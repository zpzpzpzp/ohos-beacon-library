# ohos_beacon_library 

## 简介

> 蓝牙工具，主要涉及信标区域监控以及信标设备测距

## 效果展示

![动画](动画.gif)

## 下载安装

```shell
npm install @ohos/ohos_beacon_library --save
```

OpenHarmony
npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

### 信标组件使用样例

1.在page页面中引入信标库必要的文件

```
import { Identifier, Region, BeaconParser, BeaconManager, RangeNotifier,Beacon } from '@ohos/ohos_beacon_library'
import bluetooth from '@ohos.bluetooth';
```

2.使用

```
- 启动测距
_this.beaconManager.addRangeNotifier(new class implements RangeNotifier {
    didRangeBeaconsInRegion(beacons: Array<Beacon>, region: Region): void {
        _this.beaconList = beacons
    }
});
_this.beaconManager.startRangingBeacons(region);

- 停止测距
this.beaconManager.stopRangingBeacons(new Region("myRangingUniqueId", null, null, null));

- 启动监控
_this.beaconManager.addMonitorNotifier(new class implements MonitorNotifier {
    INSIDE:number = 1
    OUTSIDE:number = 0

    didEnterRegion(region:Region):void {
        _this.enterRegionText = "I just saw a region: " + region.getUniqueId()
    }
    
    didExitRegion(region:Region):void {
        _this.enterRegionText = "I no longer see an beacon in Region:" + region.getUniqueId()
    }
    
    didDetermineStateForRegion(state:number, region:Region):void {
        _this.determineStateText = "I have just switched from seeing/not seeing beacons: " + state
    }
});
_this.beaconManager.startMonitoring(region);

- 停止监控
this.beaconManager.stopMonitoring(new Region("myMonitorUniqueId", null, null, null));
```

## 接口说明

`@State private beaconManager: BeaconManager = BeaconManager.getInstanceForApplication()`

1. 当扫描到信标时的监控通知处理接口
   `.beaconManager.addMonitorNotifier(notifier:MonitorNotifier): void`
2. 开启信标监控，查找匹配region的beacons
   `.beaconManager.startMonitoring(region:Region ):void`
3. 当扫描到信标时的距离通知处理接口
   `.beaconManager.addRangeNotifier(notifier:RangeNotifier):void`
4. 开启信标距离测算
   `.beaconManager.startRangingBeacons(region:Region):void`
5. 为信标解释器指定编码规则
   `new BeaconParser().setBeaconLayout（beaconLayout:String）:void `
6. 将常见的数据类型封装为一个Identifier类型。
   `Identifier.parse（stringValue:String, desiredByteLength?:number）:Identifier`
7. 根据uniqueId与其他参数的组合，构造匹配信标的字段条件。
   `Region(uniqueId: string, id1?: Identifier, id2?: Identifier, id3?: Identifier, identifiers?: Array<Identifier>, bluetoothAddress?: string)`

## 兼容性

支持 OpenHarmony API version 8 及以上版本。

## 目录结构

````
|---- ohos_beacon_library  
|     |---- entry  # 示例代码文件夹
|     |---- ohos_beacon_library  # beacon库
|	      |----src
|             |----main
|                 |----ets
|                     |----components
|                         |---- beaconLibrary  # beacon库文件夹
|                             |---- client #客户端三种情况数据处理
|                             |---- distance 设备相关设置及距离配置逻辑
|                             |---- logging #日志相关逻辑
|                             |---- service #服务端处理逻辑
|                             |---- simulator #信标计算
|                             |---- startup #数据发送与接收
|                             |---- utils #工具类
|                             |---- AltBeacon.ets #建立信标通信
|                             |---- BeaconParser.ets #beacon数据包解析
|                         |---- bluetooth      # 蓝牙库
|                             |---- BleAdvertisement.ets #信标广播逻辑
|                             |---- Pdu.ets #蓝牙数据传递单元
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/hihopeorg/ohos-beacon-library/issues)
给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/hihopeorg/ohos-beacon-library/pulls) 。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/hihopeorg/ohos-beacon-library/blob/master/LICENSE) ，请自由地享受和参与开源。



